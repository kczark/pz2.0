package app;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class Sagi extends Doubutsu {
	
	int skok;
	int move_material;
	int move_air;
	boolean jestNisko;
	StringBuffer gcode;
	int x;
	int y;
	int previous_gray;
	int gray;
	double abyss;
	String abyss2;
	public Sagi(Yari ya, Yuka yu, BufferedImage e, Writer f) {
		super(ya, yu, e, f);
		this.skok = getYari().getD();
		this.move_material = getYuka().getPosow_roboczy();
		this.move_air =      getYuka().getPosow_jalowy();
		this.jestNisko = false;
		this.gcode = new StringBuffer();
		this.x = 0;
		this.y = 0;
		this.previous_gray= 0;
		this.gray= 0;
		this.abyss= 0;
		this.abyss2 =""+abyss;
	}

	public void chokoku() {
		try {
			int p=255 - new Color(getManga().getRGB(x,y)).getRed();
			if(getYuka().getFrezuj_bialy()==true) {
				if(p >255) {
					abyss = getYuka().getGlebokosc_bialy();
				}
				else {
					abyss = -((p/255)*(getYuka().getGlebokosc_czarny()-getYuka().getGlebokosc_bialy())+getYuka().getGlebokosc_bialy());
				}
			}
			else {
				if(p > 255) {
					abyss = getYuka().getGlebokosc_jalowy();
				}
				else {
					abyss = -(double)((double)((double)p*(double)getYuka().getGlebokosc_czarny())/(double)255);
				}
			}
			if(abyss<0) {
				abyss2 = ""+abyss;
			}
			else if(abyss==-0){
				abyss2 = ""+abyss;
			}
			else if(abyss==0){
				abyss2 = "+"+abyss;
			}
			else {
				abyss2 = "+"+abyss;
			}
			gcode.append("G1 X+" + ((x*skok)/10) + "." + ((x*skok)%10) + " Y+" + ((y*skok)/10) + "." + ((y*skok)%10) + " Z"+abyss+" F"+move_material+"\n");
			x = 0;
			for (; x < getManga().getWidth(null);x++){
				y = 0;
				for (; y < getManga().getHeight(null); y++) {
					previous_gray = gray;
					gray=255 - new Color(getManga().getRGB(x,y)).getRed();
					if(previous_gray == 0) previous_gray = 255+255*getYuka().getGlebokosc_jalowy();
					if(gray == 0) gray = 255+255*getYuka().getGlebokosc_jalowy();
					if(getYuka().getFrezuj_bialy()==true) {
						if(gray >255) {
							abyss = getYuka().getGlebokosc_bialy();
						}
						else {
							abyss = -((gray/255)*(getYuka().getGlebokosc_czarny()-getYuka().getGlebokosc_bialy())+getYuka().getGlebokosc_bialy());
						}
					}
					else {
						if(gray > 255) {
							abyss = getYuka().getGlebokosc_jalowy();
						}
						else {
							abyss = -(double)((double)((double)gray*(double)getYuka().getGlebokosc_czarny())/(double)255);
						}
					}
					if(abyss<0) {
						abyss2 = ""+abyss;
					}
					else if(abyss==-0){
						abyss2 = ""+abyss;
					}
					else if(abyss==0){
						abyss2 = "+"+abyss;
					}
					else {
						abyss2 = "+"+abyss;
					}
					
					if (gray == previous_gray) {
						if(jestNisko == true) {
							gcode.append(here(1));
//							gcode.append("G1 Z-1 F"+move_material+"\n");
//							jestNisko = true;
						}
					}
					else if (gray > previous_gray) {
						if(jestNisko == true) {
							gcode.append(here(1));
							gcode.append("G1 Z"+abyss2+" F"+move_material+"\n");
						}
						else {
							if(getYuka().getFrezuj_bialy()==true) {
								gcode.append(here(1));
							}
							else {
								gcode.append(here(0));
							}
							gcode.append("G1 Z"+abyss2+" F"+move_material+"\n");
							jestNisko = true;
						}
					}
					else {
						if(jestNisko == true) {
							gcode.append("G1 Z"+abyss2+" F"+move_material+"\n");
							if(gray>255 && getYuka().getFrezuj_bialy()==false) {
								gcode.append(here(0));
								jestNisko = false;
							}
							else {
								gcode.append(here(1));
							}
						}
					}
//					if(y == getManga().getHeight(null)-1) {
//						if(getYuka().getFrezuj_bialy()==true) {
//							gcode.append("G0 Z+"+getYuka().getGlebokosc_bialy()+" F"+move_air+"\n");
//						}
//						else {
//							gcode.append("G0 Z+1 F"+move_air+"\n");
//							gray = 0;
//							jestNisko = false;
//						}
//					}
				}
				if(x + 1 < getManga().getWidth(null)) {
					x++;
					y = getManga().getHeight(null)-1;
					for (; y >=0; y--) {
						previous_gray = gray;
						gray= 255 - new Color(getManga().getRGB(x,y)).getRed();
						if(previous_gray == 0) previous_gray = 255+255*getYuka().getGlebokosc_jalowy();
						if(gray == 0) gray = 255+255*getYuka().getGlebokosc_jalowy();
						if(getYuka().getFrezuj_bialy()==true) {
							if(gray>255) {
								abyss=getYuka().getGlebokosc_bialy();
							}
							else {
								abyss = -((gray/255)*(getYuka().getGlebokosc_czarny()-getYuka().getGlebokosc_bialy())+getYuka().getGlebokosc_bialy());
							}
						}
						else {
							if(gray>255) {
								abyss=getYuka().getGlebokosc_jalowy();
							}
							else {
								abyss = -(double)((double)((double)gray*(double)getYuka().getGlebokosc_czarny())/(double)255);
							}
						}
						if(abyss<0) {
							abyss2 = ""+abyss;
						}
						else if(abyss==-0){
							abyss2 = ""+abyss;
						}
						else if(abyss==0){
							abyss2 = "+"+abyss;
						}
						else {
							abyss2 = "+"+abyss;
						}
						
						if (gray == previous_gray) {
							if(jestNisko == true) {
								gcode.append(here(1));
//								gcode.append("G1 Z-1 F"+move_material+"\n");
//								jestNisko = true;
							}
						}
						else if (gray > previous_gray) {
							if(jestNisko == true) {
								gcode.append(here(1));
								gcode.append("G1 Z"+abyss2+" F"+move_material+"\n");
							}
							else {
								if(getYuka().getFrezuj_bialy()==true) {
									gcode.append(here(1));
								}
								else {
									gcode.append(here(0));
								}
								gcode.append("G1 Z"+abyss2+" F"+move_material+"\n");
								jestNisko = true;
							}
						}
						else {
							if(jestNisko == true) {
								gcode.append("G1 Z"+abyss2+" F"+move_material+"\n");
								if(gray>255 && getYuka().getFrezuj_bialy()==false) {
									gcode.append(here(0));
									jestNisko = false;
								}
								else {
									gcode.append(here(1));
								}
							}
						}
//						if(y == 0) {
//							if(getYuka().getFrezuj_bialy()==true) {
//								gcode.append("G0 Z+"+getYuka().getGlebokosc_bialy()+" F"+move_air+"\n");
//							}
//							else {
//								gcode.append("G0 Z+1 F"+move_air+"\n");
//								jestNisko = false;
//							}
//						}
					}
					
				}
			}
			// Wrzucenie do pliku
			getKartka().append(gcode.toString());
			getKartka().flush();

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private String here(int g) {
		String tmp = "";
		if(g == 0) {
			tmp = "G0 X+" + ((x*skok)/10) + "." + ((x*skok)%10) + " Y+" + (((getManga().getHeight() - y)*skok)/10) + "." + (((getManga().getHeight() - y)*skok)%10) + " F"+move_air+"\n";
		}
		else if(g == 1) {
			tmp = "G1 X+" + ((x*skok)/10) + "." + ((x*skok)%10) + " Y+" + (((getManga().getHeight() - y)*skok)/10) + "." + (((getManga().getHeight() - y)*skok)%10) + " F"+move_material+"\n";
		}
		return tmp;
	}
}
