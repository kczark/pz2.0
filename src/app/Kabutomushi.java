package app;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

class Tuple<X, Y> {
    public final X x;
    public final Y y;
    Tuple(X x, Y y) {
        this.x = x;
        this.y = y;
    }
}


class The_Comparator implements Comparator<Tuple<Integer,Integer>> {
    public int compare(Tuple<Integer,Integer> str1, Tuple<Integer,Integer> str2)
    {
        float dist_1 = (str1.x-Kabutomushi.global_x)*(str1.x-Kabutomushi.global_x)+(str1.y-Kabutomushi.global_y)*(str1.y-Kabutomushi.global_y);
        float dist_2 = (str2.x-Kabutomushi.global_x)*(str2.x-Kabutomushi.global_x)+(str2.y-Kabutomushi.global_y)*(str2.y-Kabutomushi.global_y);
        return (int)(dist_1-dist_2);
    }
}

public class Kabutomushi extends Doubutsu {
    public Kabutomushi(Yari ya, Yuka yu, BufferedImage e, Writer f){ super(ya,yu,e,f); }

    private int     [][] results = new int    [getManga().getWidth(null)+2][getManga().getHeight(null)+2];
    private boolean [][] done    = new boolean[getManga().getWidth(null)+2][getManga().getHeight(null)+2];
    private ArrayList<Integer> counts = new ArrayList<>(0);
    private Stack<Tuple<Integer,Integer>> dfs_stack = new Stack<>();

    static int global_x;
    static int global_y;

    private int dfs(int x, int y, int id){
        long r = (getManga().getRGB(x, getManga().getHeight(null)-y-1) & 0x00ff0000) >> (2 * 8);
        long g = (getManga().getRGB(x, getManga().getHeight(null)-y-1) & 0x0000ff00) >> (    8);
        long b = (getManga().getRGB(x, getManga().getHeight(null)-y-1) & 0x000000ff) >> (    0);
        long brightness = (r + g + b) / 3;
        if (!(brightness < 200 && results[x + 1][y + 1] == 0)) {
            return 0;
        }
        Tuple<Integer,Integer> a = new Tuple<>(x,y);
        dfs_stack.push(a);
        while(!dfs_stack.empty()) {
            a = dfs_stack.pop();
            x = a.x;
            y = a.y;
            r = (getManga().getRGB(x, getManga().getHeight(null)-y-1) & 0x00ff0000) >> (2 * 8);
            g = (getManga().getRGB(x, getManga().getHeight(null)-y-1) & 0x0000ff00) >> (    8);
            b = (getManga().getRGB(x, getManga().getHeight(null)-y-1) & 0x000000ff) >> (    0);
            brightness = (r + g + b) / 3;
            if (brightness < 200 && results[x + 1][y + 1] == 0) {
                results[x + 1][y + 1] = id;
                done[x + 1][y + 1] = false;
                if (id > counts.size()) {
                    counts.add(0);
                }
                counts.set(id - 1, counts.get(id - 1) + 1);
                if (x > 0) {
                    a = new Tuple<>(x - 1,y);
                    dfs_stack.push(a);
                    if (y > 0) {
                        a = new Tuple<>(x - 1,y - 1);
                        dfs_stack.push(a);
                    }
                    if (y < getManga().getHeight(null) - 1) {
                        a = new Tuple<>(x - 1,y + 1);
                        dfs_stack.push(a);
                    }
                }
                if (y > 0) {
                    a = new Tuple<>(x,y - 1);
                    dfs_stack.push(a);
                }
                if (x < getManga().getWidth(null) - 1) {
                    a = new Tuple<>(x + 1,y);
                    dfs_stack.push(a);
                    if (y > 0) {
                        a = new Tuple<>(x + 1,y - 1);
                        dfs_stack.push(a);
                    }
                    if (y < getManga().getHeight(null) - 1) {
                        a = new Tuple<>(x + 1,y + 1);
                        dfs_stack.push(a);
                    }
                }
                if (y < getManga().getHeight(null) - 1) {
                    a = new Tuple<>(x,y + 1);
                    dfs_stack.push(a);
                }
            }
        }
        return 1;
    }

    private Tuple<Integer,Integer> dijkstra(int x, int y){
        PriorityQueue<Tuple<Integer,Integer>> my_queue = new PriorityQueue<>(10,new The_Comparator());
        boolean [][] visited = new boolean [getManga().getWidth(null)+2][getManga().getHeight(null)+2];
        for (int i = 0; i < getManga().getWidth(null)+2; i++) {
            for (int j = 0; j < getManga().getHeight(null)+2; j++) {
                visited[i][j] = false;
            }
        }
        Kabutomushi.global_x=x;
        Kabutomushi.global_y=y;
        Tuple<Integer,Integer> a = new Tuple<>(x,y);
        my_queue.add(a);
        while(!my_queue.isEmpty()){
            a = my_queue.remove();
            if(a.x<1||a.x>getManga().getWidth(null)||a.y<1||a.y>getManga().getHeight(null)){
                continue;
            }
            if(!visited[a.x][a.y]){
                visited[a.x][a.y]=true;
                if(!done[a.x][a.y]){
                    return a;
                }
                if(!visited[a.x-1][a.y])my_queue.add(new Tuple<>(a.x - 1, a.y));
                if(!visited[a.x+1][a.y])my_queue.add(new Tuple<>(a.x+1,a.y));
                if(!visited[a.x][a.y-1])my_queue.add(new Tuple<>(a.x,a.y-1));
                if(!visited[a.x][a.y+1])my_queue.add(new Tuple<>(a.x,a.y+1));
            }
        }
        return new Tuple<>(-1,-1);
    }

    public void chokoku() {
        double skok = getYari().getD() / 10.0;
        int speed_material  =  getYuka().getPosow_roboczy();
        int height_material = -getYuka().getGlebokosc_czarny();
        int speed_air  =       getYuka().getPosow_jalowy();
        int height_air =       getYuka().getGlebokosc_jalowy();
        StringBuilder gcode = new StringBuilder();


        for(int x = 0; x <= getManga().getWidth(null)+1; x++){
            for(int y = 0; y <= getManga().getHeight(null)+1; y++) {
                results[x][y] = 0;
                done   [x][y] = true;
            }
        }

        try {

            int id=1;
            for (int x = 0; x < getManga().getWidth(null); x++) {
                for (int y = 0; y < getManga().getHeight(null); y++) {
                    if(dfs(x,y,id)==1){
                        id++;
                    }
                    //System.out.print(results[x][y]);
                    //System.out.print(' ');
                }
                //System.out.print('\n');
            }

            int x = 1, y = 1;
            int [][] directions = {{-1,0},{-1,1},{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1}};

            Tuple <Integer,Integer> a;

            gcode.append(";;Początek Kabutomushi\n");
            gcode.append("G0 Z+").append(String.format ("%d"  , height_air));
            gcode.append(  " F" ).append(String.format ("%d"  , speed_material)).append("\n");

            a = dijkstra(x,y);
            while(a.x!=-1&&a.y!=-1){
                x = a.x;
                y = a.y;
                int x_new, y_new;
                gcode.append("G0 X+").append(String.format ("%.2f", x * skok));
                gcode.append(  " Y+").append(String.format ("%.2f", y * skok));
                gcode.append(  " Z+").append(String.format ("%d"  , height_air));
                gcode.append(  " F" ).append(String.format ("%d"  , speed_air)).append("\n");

                gcode.append("G1 Z" ).append(String.format ("%d"  , height_material));
                gcode.append(  " F" ).append(String.format ("%d"  , speed_material)).append("\n");
                done[x][y] = true;
                int i = results[x][y];
                boolean stop = false;
                int direction = 0;
                int direction_new = 0;
                x_new = x;
                y_new = y;
                while (!stop) {
                    x = x_new;
                    y = y_new;
                    direction=direction_new;
                    counts.set(i - 1, counts.get(i - 1) - 1);
                    done[x][y] = true;

                    int count=0;
                    while(count<8){
                        direction_new--;
                        direction_new+=8;
                        direction_new%=8;
                        count++;
                        x_new = x+directions[direction_new][0];
                        y_new = y+directions[direction_new][1];
                        if(results[x_new][y_new]!=i||done[x_new][y_new]){
                            break;
                        }
                    }

                    count=0;
                    while(count<8){
                        direction_new++;
                        direction_new%=8;
                        count++;
                        x_new = x+directions[direction_new][0];
                        y_new = y+directions[direction_new][1];
                        if(results[x_new][y_new]==i&&!done[x_new][y_new]){
                            break;
                        }
                    }
                    if(count==8) stop = true;
                    if(direction_new!=direction) {
                        gcode.append("G1 X+").append(String.format ("%.2f", x * skok));
                        gcode.append(  " Y+").append(String.format ("%.2f", y * skok));
                        gcode.append(  " Z" ).append(String.format ("%d"  , height_material));
                        gcode.append(  " F" ).append(String.format ("%d"  , speed_material)).append("\n");
                    }
                }
                if(direction_new==direction) {
                    gcode.append("G1 X+").append(String.format ("%.2f", x * skok));
                    gcode.append(  " Y+").append(String.format ("%.2f", y * skok));
                    gcode.append(  " Z" ).append(String.format ("%d"  , height_material));
                    gcode.append(  " F" ).append(String.format ("%d"  , speed_material)).append("\n");
                }

                gcode.append("G0 Z+").append(String.format ("%d"  , height_air));
                gcode.append(  " F" ).append(String.format ("%d"  , speed_material)).append("\n");

                a = dijkstra(x,y);
            }

            gcode.append(";;Koniec Kabutomushi\n");

            // Wrzucenie do pliku
            getKartka().write(gcode.toString());
            getKartka().flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
