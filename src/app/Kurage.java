package app;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;


public class Kurage extends Doubutsu {

	private double skok;

	public Kurage(Yari ya, Yuka yu, BufferedImage e, Writer f) {
		super(ya, yu, e, f);

		skok = 0.05 * getYari().getD();
	}

	
	public void chokoku() {

		StringBuilder wyj = new StringBuilder(); // tworzenie napisu z kodem

		try {
			
			ArrayList<Double[]> lista= new ArrayList<>();

			lista.add(new Double[] { 0.0d, 0.0d, 0.1*getManga().getWidth(null)* getYari().getD(), 0.1*getManga().getHeight(null)* getYari().getD() });

			while(((lista.get(0)[3]-lista.get(0)[1])*(lista.get(0)[3]-lista.get(0)[1])+(lista.get(0)[2]-lista.get(0)[0])*(lista.get(0)[2]-lista.get(0)[0]))>4.0*skok*skok) {
				
				ArrayList<Double[]> lista_tmp= new ArrayList<>();


				for (Double[] doubles : lista) {

					Double x_pocz = doubles[0];
					Double y_pocz = doubles[1];

					Double x_konc = doubles[2];
					Double y_konc = doubles[3];


					Double x_delta = (x_konc - x_pocz) / 3.0;
					Double y_delta = (y_konc - y_pocz) / 3.0;


					double x_Q1 = x_pocz + x_delta;
					double y_Q1 = y_pocz + y_delta;

					double x_Q2 = x_Q1 + x_delta;
					double y_Q2 = y_Q1 + y_delta;


					lista_tmp.add(new Double[]{x_pocz, y_pocz, x_Q1, y_Q1});
					lista_tmp.add(new Double[]{x_Q1, y_Q1, x_pocz, y_Q2});
					lista_tmp.add(new Double[]{x_pocz, y_Q2, x_Q1, y_konc});


					lista_tmp.add(new Double[]{x_Q1, y_konc, x_Q2, y_Q2});
					lista_tmp.add(new Double[]{x_Q2, y_Q2, x_Q1, y_Q1});


					lista_tmp.add(new Double[]{x_Q1, y_Q1, x_Q2, y_pocz});

					lista_tmp.add(new Double[]{x_Q2, y_pocz, x_konc, y_Q1});
					lista_tmp.add(new Double[]{x_konc, y_Q1, x_Q2, y_Q2});


					lista_tmp.add(new Double[]{x_Q2, y_Q2, x_konc, y_konc});
				}
				
				lista=lista_tmp;
			}


			for (Double[] doubles : lista) {
				wyj.append("G1 X+").append(doubles[2]);
				wyj.append(  " Y+").append(doubles[3]);
				wyj.append(  " F" ).append(this.getYuka().getPosow_roboczy()).append("\n");
			}


			wyj.append("G0 Z+").append(String.format ("%d"  , this.getYuka().getGlebokosc_jalowy()));
			wyj.append(  " F" ).append(String.format ("%d"  , this.getYuka().getPosow_jalowy())).append("\n");

			wyj.append("G0 X+0.0");
			wyj.append(  " Y+0.0").append("\n");

			wyj.append("G0 Z+0.0").append("\n");
			wyj.append(  " F" ).append(String.format ("%d"  , this.getYuka().getPosow_jalowy())).append("\n");


			// Wrzucenie do pliku
			getKartka().append(wyj.toString());
			getKartka().flush();

		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}



}