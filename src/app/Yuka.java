package app;

public class Yuka {
    private int posow_roboczy = 20;
    private int posow_jalowy = 200;
    private int predkosc_obrotowa = 12;


    private int szer_ramki=2;
    private int glebokosc_ramki=20;
    private Boolean ramka=true;
    private int wygladzanie=0;  // 0 - bez wygladzania
                                // 1 - Liniowe
                                // 2 Spiralne
                                // 3 skośne
                                // 4 fraktal Peano

    private Boolean frezuj_bialy=false;
    private int glebokosc_bialy=0;
    private int glebokosc_czarny=0;
    private int glebokosc_jalowy=0;


    public int  getGlebokosc_bialy ()                     { return glebokosc_bialy;  }
    public void setGlebokosc_bialy (int glebokosc_bialy ) {   this.glebokosc_bialy  = glebokosc_bialy;  }
    public int  getGlebokosc_czarny()                     { return glebokosc_czarny; }
    public void setGlebokosc_czarny(int glebokosc_czarny) {   this.glebokosc_czarny = glebokosc_czarny; }
    public int  getGlebokosc_jalowy()                     { return glebokosc_jalowy; }
    public void setGlebokosc_jalowy(int glebokosc_jalowy) {   this.glebokosc_jalowy = glebokosc_jalowy; }

    public Boolean getFrezuj_bialy()                     { return frezuj_bialy; }
    public void    setFrezuj_bialy(Boolean frezuj_bialy) {   this.frezuj_bialy = frezuj_bialy; }

    public int  getPredkosc_obrotowa()                      { return predkosc_obrotowa; }
    public void setPredkosc_obrotowa(int predkosc_obrotowa) {   this.predkosc_obrotowa = predkosc_obrotowa; }

    public int  getWygladzanie()                { return wygladzanie; }
    public void setWygladzanie(int wygladzanie) {   this.wygladzanie = wygladzanie; }

    public int  getPosow_roboczy  ()                    { return posow_roboczy; }
    public void setPosow_roboczy  (int posow_roboczy)   {   this.posow_roboczy   = posow_roboczy;   }
    public int  getPosow_jalowy   ()                    { return posow_jalowy;  }
    public void setPosow_jalowy   (int posow_jalowy)    {   this.posow_jalowy    = posow_jalowy;    }
	public int  getSzer_ramki     ()                    { return szer_ramki;    }
	public void setSzer_ramki     (int szer_ramki)      {   this.szer_ramki      = szer_ramki;      }
	public int  getGlebokosc_ramki()                    { return glebokosc_czarny; }
	public void setGlebokosc_ramki(int glebokosc_ramki) {   this.glebokosc_ramki = glebokosc_ramki; }

	public Boolean getRamka() {
		return ramka;
	}
	public void    setRamka(Boolean ramka) {
		this.ramka = ramka;
	}

}
