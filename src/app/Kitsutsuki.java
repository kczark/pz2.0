package app;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class Kitsutsuki extends Doubutsu {

	public Kitsutsuki(Yari ya, Yuka yu, BufferedImage e, Writer f) {
		super(ya, yu, e, f);
	}

	public void chokoku() {
		int skok = getYari().getD();
		// int wys = getYari().getH();

		try {
			if (getYuka().getFrezuj_bialy()) {

				int wys = this.getYuka().getGlebokosc_czarny() - this.getYuka().getGlebokosc_bialy();
				for (int x = 0; x < getManga().getWidth(null); x++) {
					for (int y = 0; y < getManga().getHeight(null); y++) {
						int am = getManga().getRGB(x, y);
						Color col = new Color(am);
						int r = col.getRed();
						int g = col.getGreen();
						int b = col.getBlue();
						double szary = (double) ((double)0.2989 * (double)r + (double)0.4870 * (double)g + (double)0.2141 * (double)b);
						{
							getKartka().append(
									"G0 X+" + ((x * skok) / 10) + "." + ((x * skok) % 10) + " Y+" + ((y * skok) / 10)
											+ "." + ((y * skok) % 10) + " Z+" + getYuka().getGlebokosc_jalowy() + " F+"
											+ this.getYuka().getPosow_jalowy() + "\n");
							getKartka().append("G1 Z-" + szary * wys
									+ " F" + this.getYuka().getPosow_roboczy() + "\n"); // +((255-szary)*wys/255)
																						// +"."+((szary*wys)%255)
							getKartka().append("G0 Z+" + getYuka().getGlebokosc_jalowy() + " F"
									+ this.getYuka().getPosow_jalowy() + "\n");
						}
					}
				}

			} else {
				int wys = this.getYuka().getGlebokosc_czarny();
				for (int x = 0; x < getManga().getWidth(null); x++) {
					for (int y = 0; y < getManga().getHeight(null); y++) {
						// int p = getObraz().getRGB(x, y);
						// int rgb = p & 0x000000ff;
						int am = getManga().getRGB(x, y);
						Color col = new Color(am);
						int r = col.getRed();
						int g = col.getGreen();
						int b = col.getBlue();
						int szary = (int) (0.2989 * r + 0.4870 * g + 0.2141 * b);
						


						getKartka().append(
								"G0 X+" + ((x * skok) / 10) + "." + ((x * skok) % 10) + " Y+" + ((y * skok) / 10)
										+ "." + ((y * skok) % 10) + " Z+" + getYuka().getGlebokosc_jalowy() + " F+"
										+ this.getYuka().getPosow_jalowy() + "\n");
						getKartka().append("G1 Z-" + szary * wys
								+ " F" + this.getYuka().getPosow_roboczy() + "\n"); // +((255-szary)*wys/255)
																					// +"."+((szary*wys)%255)
						getKartka().append("G0 Z+" + getYuka().getGlebokosc_jalowy() + " F"
								+ this.getYuka().getPosow_jalowy() + "\n");
					}
					}
				}

			// sekwencja kończąca

			// Wrzucenie do pliku

			getKartka().flush();

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
