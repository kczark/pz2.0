package app;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class Rei extends Doubutsu {

	private double skok;

	public Rei(Yari ya, Yuka yu, BufferedImage e, Writer f) {
		super(ya, yu, e, f);

		skok = 0.05 * getYari().getD();
	}

	public void chokoku() {

		StringBuilder wyj = new StringBuilder(); // tworzenie napisu z kodem

		try {

			for (int i = 0; i <=getManga().getWidth(null) ; i++) {
				wyj.append("G1 X+").append((2 * i) * skok);
				wyj.append(" F").append(this.getYuka().getPosow_roboczy()).append("\n");
				
				wyj.append("G1 Y+").append(2 * (getManga().getHeight(null)) * skok);
				wyj.append(  " F" ).append(this.getYuka().getPosow_roboczy()).append("\n");

				wyj.append("G1 X+").append((2 * i + 1) * skok);
				wyj.append(" F"   ).append(this.getYuka().getPosow_roboczy()).append("\n");
				
				wyj.append("G1 Y+0.0 F").append(this.getYuka().getPosow_roboczy()).append("\n");
			}

			wyj.append("G0 Z+").append(String.format ("%d"  , this.getYuka().getGlebokosc_jalowy()));
			wyj.append(  " F" ).append(String.format ("%d"  , this.getYuka().getPosow_jalowy())).append("\n");

			wyj.append("G0 X+0.0");
			wyj.append(  " Y+0.0").append("\n");

			wyj.append("G0 Z+0.0").append("\n");


			// Wrzucenie do pliku
			getKartka().append(wyj.toString());
			getKartka().flush();

		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}