
package app;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.*;

import static java.lang.Math.abs;
import static java.lang.Math.min;

public class Controller {

    private BufferedImage obraz = null;
    private BufferedImage obraz_wyswietlanie = null;
    private Yari yari = new Yari();
    private Yuka yuka = new Yuka();
    private File selectedPfile=new File("Dane/kolor.jpg");
    private int wygladzenie=0;

    
    @FXML
    private ImageView obrazek;
    @FXML
    private TextField jalowy_tekstfield,bialy_tekstfield,czarny_tekstfield,maksymalna_ilosc_pikseli_textfield,
            ilosc_kolorow_na_obrazku_textfield,predkosc_obrotowa_textfield,posow_roboczy_textfield,
            posow_jalowy_textfield,wysokosc_robocza_freza_textfield,kat_rozwarcia_freza_textfield,
            srednica_czola_freza_textfield;
    @FXML
    private TextArea gcode_pole;
    @FXML
    private CheckBox ramka_checkbox,bialy_checkbox;
    @FXML
    private Text sciezka_tekst,wymiary_tekst;
    @FXML
    private MenuButton menubutton;

    @FXML
    public void initialize(){

        bialy_tekstfield.setTextFormatter(new TextFormatter<String>( change -> {
            change.setText(change.getText().replaceAll("[^0-9.,-]", ""));
            return change;
        }));
        jalowy_tekstfield.setTextFormatter(new TextFormatter<String>( change -> {
            change.setText(change.getText().replaceAll("[^0-9.,-]", ""));
            return change;
        }));
        czarny_tekstfield.setTextFormatter(new TextFormatter<String>( change -> {
            change.setText(change.getText().replaceAll("[^0-9.,-]", ""));
            return change;
        }));
        maksymalna_ilosc_pikseli_textfield.setTextFormatter(new TextFormatter<String>( change -> {
            change.setText(change.getText().replaceAll("[^0-9]", ""));
            return change;
        }));
        ilosc_kolorow_na_obrazku_textfield.setTextFormatter(new TextFormatter<String>( change -> {
            change.setText(change.getText().replaceAll("[^0-9]", ""));
            return change;
        }));
        predkosc_obrotowa_textfield.setTextFormatter(new TextFormatter<String>( change -> {
            change.setText(change.getText().replaceAll("[^0-9]", ""));
            return change;
        }));
        posow_roboczy_textfield.setTextFormatter(new TextFormatter<String>( change -> {
            change.setText(change.getText().replaceAll("[^0-9]", ""));
            return change;
        }));
        posow_jalowy_textfield.setTextFormatter(new TextFormatter<String>( change -> {
            change.setText(change.getText().replaceAll("[^0-9]", ""));
            return change;
        }));
        wysokosc_robocza_freza_textfield.setTextFormatter(new TextFormatter<String>( change -> {
            change.setText(change.getText().replaceAll("[^0-9.,]", ""));
            return change;
        }));
        kat_rozwarcia_freza_textfield.setTextFormatter(new TextFormatter<String>( change -> {
            change.setText(change.getText().replaceAll("[^0-9]", ""));
            return change;
        }));
        srednica_czola_freza_textfield.setTextFormatter(new TextFormatter<String>( change -> {
            change.setText(change.getText().replaceAll("[^0-9.,]", ""));
            return change;
        }));





        try {File file = new File("Dane/USTAWIENIA_NIE_RUSZAC.nietopyrz");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;
            int i=0;
            String[] tablica = new String[100];
            while ((st = br.readLine()) != null){
                tablica[i]=st;
                i++;
            }
            srednica_czola_freza_textfield.setText(tablica[0]);
            kat_rozwarcia_freza_textfield.setText(tablica[1]);
            wysokosc_robocza_freza_textfield.setText(tablica[2]);
            posow_jalowy_textfield.setText(tablica[3]);
            posow_roboczy_textfield.setText(tablica[4]);
            predkosc_obrotowa_textfield.setText(tablica[5]);
            ilosc_kolorow_na_obrazku_textfield.setText(tablica[6]);
            maksymalna_ilosc_pikseli_textfield.setText(tablica[7]);
            czarny_tekstfield.setText(tablica[8]);
            jalowy_tekstfield.setText(tablica[9]);
            bialy_tekstfield.setText(tablica[10]);
            ramka_checkbox.setSelected(Boolean.valueOf(tablica[11]));
            switch (tablica[12]) {
                case "0":
                    Label t = new Label("Brak");
                	t.setStyle("-fx-text-fill:#e2e2e2;");
                    menubutton.setGraphic(t);
                    wygladzenie=0;
                    break;
                case "1":
                	Label zt = new Label("Rei レイ (Płaszczka)");
                	zt.setStyle("-fx-text-fill:#e2e2e2;");
                    menubutton.setGraphic(zt);
                    wygladzenie=1;
                    break;
                case "2":
                	Label a = new Label("Makigai 巻き貝 (Ślimak)");
                	a.setStyle("-fx-text-fill:#e2e2e2;");
                    menubutton.setGraphic(a);
                    wygladzenie=2;
                    break;
                case "3":
                	Label b = new Label("Kemushi 毛虫 (Gąsienica)");
                	b.setStyle("-fx-text-fill:#e2e2e2;");
                    menubutton.setGraphic(b);
                    wygladzenie=3;
                    break;
                case "4":
                	Label c = new Label("Kurage 海月 (Meduza)");
                	c.setStyle("-fx-text-fill:#e2e2e2;");
                    menubutton.setGraphic(c);
                    wygladzenie=4;
                    break;
            }

            bialy_checkbox.setSelected(Boolean.valueOf(tablica[13]));
            selectedPfile=new File(tablica[14]);
            zmiana_parametrow_obrazu();
        }
        catch (Exception aa){
            srednica_czola_freza_textfield.setText("0.2");
            kat_rozwarcia_freza_textfield.setText("22");
            wysokosc_robocza_freza_textfield.setText("33");
            posow_jalowy_textfield.setText("1000");
            posow_roboczy_textfield.setText("50");
            predkosc_obrotowa_textfield.setText("222");
            ilosc_kolorow_na_obrazku_textfield.setText("2");
            maksymalna_ilosc_pikseli_textfield.setText("16");
            czarny_tekstfield.setText("1");
            jalowy_tekstfield.setText("1");
            bialy_tekstfield.setText("1");
            ramka_checkbox.setSelected(false);
            Label t = new Label("Brak");
            t.setStyle("-fx-text-fill:#e2e2e2;");
            menubutton.setGraphic(t);
            wygladzenie=0;
            bialy_checkbox.setSelected(false);
            selectedPfile=new File("Dane/kolor.jpg");
            zapis();
            zmiana_parametrow_obrazu();
        }

        ilosc_kolorow_na_obrazku_textfield.textProperty().addListener((obj, oldVal, newVal) ->zmiana_parametrow_obrazu());
        srednica_czola_freza_textfield.textProperty().addListener((obj, oldVal, newVal) ->zmiana_parametrow_obrazu());
        maksymalna_ilosc_pikseli_textfield.textProperty().addListener((obj, oldVal, newVal) ->zmiana_parametrow_obrazu());

    }


    public void wgraj_zdjecie_przycisk_onclick(ActionEvent event){
        zapis();
        JFileChooser chooser = new JFileChooser();
        chooser.setAcceptAllFileFilterUsed(false);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("画像ファイル (Image Files)", "bmp", "jpg", "jpeg", "wbmp", "png", "gif");
        chooser.addChoosableFileFilter(filter);
        chooser.setCurrentDirectory(new File("."));
        chooser.setDialogTitle("Wybierz obraz");
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.showOpenDialog(null);
        selectedPfile = chooser.getSelectedFile();
        try {
            obraz = ImageIO.read(new File(selectedPfile.getAbsolutePath()));
            obraz_wyswietlanie = obraz;
        }
        catch (IOException e){
            e.printStackTrace();
        }
        obraz = resize(obraz,maksymalna_ilosc_pikseli_textfield.getText());
        obraz=filtrbw(obraz.getWidth(),obraz.getHeight(),obraz,Integer.parseInt(ilosc_kolorow_na_obrazku_textfield.getText())-1);
        obraz_wyswietlanie = resize(obraz,"300");
        obraz_wyswietlanie=filtrbw(obraz_wyswietlanie.getWidth(),obraz_wyswietlanie.getHeight(),obraz_wyswietlanie,Integer.parseInt(ilosc_kolorow_na_obrazku_textfield.getText())-1);
        obrazek.setImage(SwingFXUtils.toFXImage(obraz_wyswietlanie,null));
        sciezka_tekst.setText(selectedPfile.getPath());
        wymiary_tekst.setText(String.format("%.3f",(obraz.getWidth()*Double.parseDouble(srednica_czola_freza_textfield.getText())))+"mm"+" x "+
        		String.format("%.3f",(obraz.getHeight()*Double.parseDouble(srednica_czola_freza_textfield.getText())))+"mm");
    }

    public void kabutomushi_przycisk_onclick(ActionEvent event){
        zapis();
        JFrame parentFrame = new JFrame();
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Specify a file to save");
        fileChooser.setSelectedFile(new File("plik.nc"));
        fileChooser.showSaveDialog(parentFrame);
        ustawiacz();
        try {
            Kabutomushi kabutomushi = new Kabutomushi(yari, yuka, obraz, new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileChooser.getSelectedFile()))));
            kabutomushi.Chokoku();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            File file = new File(fileChooser.getSelectedFile().getAbsolutePath());
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st,koniec="";
            while ((st = br.readLine()) != null){
                koniec+=st;
                koniec+="\n";
            }
            gcode_pole.setText(koniec);
        }
        catch (IOException aa) {
        }
    }

    public void kitsutsuki_przycisk_onclick(ActionEvent event){
        zapis();
        JFrame parentFrame = new JFrame();
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Specify a file to save");
        fileChooser.setSelectedFile(new File("plik.nc"));
        fileChooser.showSaveDialog(parentFrame);
        ustawiacz();
        try {
            Kitsutsuki kitsutsuki = new Kitsutsuki(yari, yuka, obraz, new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileChooser.getSelectedFile()))));
            kitsutsuki.Chokoku();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            File file = new File(fileChooser.getSelectedFile().getAbsolutePath());
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st,koniec="";
            while ((st = br.readLine()) != null){
                koniec+=st;
                koniec+="\n";
            }
            gcode_pole.setText(koniec);
        }
        catch (IOException aa) {
        }
    }

    public void sagi_przycisk_onclick(ActionEvent event){
        zapis();
        JFrame parentFrame = new JFrame();
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Specify a file to save");
        fileChooser.setSelectedFile(new File("plik.nc"));
        fileChooser.showSaveDialog(parentFrame);
        ustawiacz();
        try {
            Sagi sagi = new Sagi(yari, yuka, obraz, new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileChooser.getSelectedFile()))));
            sagi.Chokoku();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            File file = new File(fileChooser.getSelectedFile().getAbsolutePath());
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st,koniec="";
            while ((st = br.readLine()) != null){
                koniec+=st;
                koniec+="\n";
            }
            gcode_pole.setText(koniec);
        }
        catch (IOException aa) {
        }
    }
    
    public void help_przycisk_onclick(ActionEvent event){
        zapis();
        try {
            File file = new File("Dane/slownik.txt");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st,koniec="";
            while ((st = br.readLine()) != null){
                koniec+=st;
                koniec+="\n";
            }
            gcode_pole.setText(koniec);
        }
        catch (IOException aa) {
        }
        
    }

    public void brak_menuitem_onclick(ActionEvent event){
    	Label t = new Label("Brak");
    	t.setStyle("-fx-text-fill:#e2e2e2;");
        menubutton.setGraphic(t);
        wygladzenie=0;
    }

    public void liniowe_menuitem_onclick(ActionEvent event){
    	Label t = new Label("Rei レイ (Płaszczka)");
    	t.setStyle("-fx-text-fill:#e2e2e2;");
        menubutton.setGraphic(t);
        wygladzenie=1;
    }

    public void spiralne_menuitem_onclick(ActionEvent event){
    	Label t = new Label("Makigai 巻き貝 (Ślimak)");
    	t.setStyle("-fx-text-fill:#e2e2e2;");
        menubutton.setGraphic(t);
        wygladzenie=2;
    }
    public void skosne_menuitem_onclick(ActionEvent event){
    	Label t = new Label("Kemushi 毛虫 (Gąsienica)");
    	t.setStyle("-fx-text-fill:#e2e2e2;");
        menubutton.setGraphic(t);
        wygladzenie=3;
    }

    public void peano_menuitem_onclick(ActionEvent event){
    	Label t = new Label ("Kurage 海月 (Meduza)");
    	t.setStyle("-fx-text-fill:#e2e2e2;");
        menubutton.setGraphic(t);
        wygladzenie=4;
    }

    public BufferedImage filtrbw(int width, int height, BufferedImage im, int ilosckolorow) {
        BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster raster = image.getRaster();
        ColorModel model = im.getColorModel();
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
            {
                int am=im.getRGB(i,j);
                Color col = new Color(am);
                int r=col.getRed();
                int g=col.getGreen();
                int b=col.getBlue();
                int szary = (int)(0.2989*r+0.4870*g+0.2141*b);
                int result=1000;
                for (int ii=0;ii<=512;ii+=256/ilosckolorow) {
                    if (abs(szary - result) > abs(szary - ii))
                        result = ii;
                }
                szary=min(result,255);
                Color kolor = new Color(szary,szary,szary);
                Object cKolor = model.getDataElements(kolor.getRGB(),null);
                raster.setDataElements(i,j,cKolor);

            }
        return image;
    }

    public BufferedImage resize(BufferedImage inputImage,String max_pix){
        BufferedImage outputImage=null;
        int max_pikseli = Integer.parseInt(max_pix);
        if(inputImage.getHeight()>inputImage.getWidth())
        {

            outputImage = new BufferedImage((max_pikseli*inputImage.getWidth())/inputImage.getHeight(),
                    max_pikseli, inputImage.getType());
            Graphics2D g2d = outputImage.createGraphics();
            g2d.drawImage(inputImage, 0, 0,
                    (max_pikseli*inputImage.getWidth())/inputImage.getHeight(), max_pikseli, null);
            g2d.dispose();
        }
        else
        {
            outputImage = new BufferedImage(max_pikseli,(max_pikseli*inputImage.getHeight())/inputImage.getWidth()
                    , inputImage.getType());
            Graphics2D g2d = outputImage.createGraphics();
            g2d.drawImage(inputImage, 0, 0,
                    max_pikseli, (max_pikseli*inputImage.getHeight())/inputImage.getWidth()
                    , null);
            g2d.dispose();
        }


        return outputImage;
    }

    public void ustawiacz(){
            yari.setD((int)(Double.parseDouble(srednica_czola_freza_textfield.getText())*10));
            yari.setAlpha(Integer.parseInt(kat_rozwarcia_freza_textfield.getText()));
            yari.setH(Integer.parseInt(wysokosc_robocza_freza_textfield.getText()));
            yuka.setPosow_jalowy(Integer.parseInt(posow_jalowy_textfield.getText()));
            yuka.setPosow_roboczy(Integer.parseInt(posow_roboczy_textfield.getText()));
            yuka.setPredkosc_obrotowa(Integer.parseInt(predkosc_obrotowa_textfield.getText())*1000);
            yuka.setRamka(ramka_checkbox.isSelected());
            yuka.setWygladzanie(wygladzenie);
            yuka.setFrezuj_bialy(bialy_checkbox.isSelected());
            yuka.setGlebokosc_bialy(Integer.parseInt(bialy_tekstfield.getText()));
            yuka.setGlebokosc_czarny(Integer.parseInt(czarny_tekstfield.getText()));
            yuka.setGlebokosc_jalowy(Integer.parseInt(jalowy_tekstfield.getText()));
    }

    public void zapis() {
        try {
            String str = "";
            BufferedWriter writer = new BufferedWriter(new FileWriter(new File("Dane/USTAWIENIA_NIE_RUSZAC.nietopyrz")));

            str+=srednica_czola_freza_textfield.getText();
            str+="\n";
            str+=kat_rozwarcia_freza_textfield.getText();
            str+="\n";
            str+=wysokosc_robocza_freza_textfield.getText();
            str+="\n";
            str+=posow_jalowy_textfield.getText();
            str+="\n";
            str+=posow_roboczy_textfield.getText();
            str+="\n";
            str+=predkosc_obrotowa_textfield.getText();
            str+="\n";
            str+=ilosc_kolorow_na_obrazku_textfield.getText();
            str+="\n";
            str+=maksymalna_ilosc_pikseli_textfield.getText();
            str+="\n";
            str+=czarny_tekstfield.getText();
            str+="\n";
            str+=jalowy_tekstfield.getText();
            str+="\n";
            str+=bialy_tekstfield.getText();
            str+="\n";
            str+=ramka_checkbox.isSelected();
            str+="\n";
            str+=Integer.toString(wygladzenie);
            str+="\n";
            str+=bialy_checkbox.isSelected();
            str+="\n";
            str+=selectedPfile.getAbsolutePath();
            writer.write(str);
            writer.close();
            System.out.println("Zapisano");
        }
        catch (IOException ee) {
            ee.printStackTrace();
        }
    }

    public void zmiana_parametrow_obrazu(){
        {
            zapis();
            try {
                obraz = ImageIO.read(new File(selectedPfile.getAbsolutePath()));
                obraz_wyswietlanie = obraz;
            }
            catch (IOException ee)
            {
                try{
                    selectedPfile = new File("Dane/kolor.jpg");
                    obraz = ImageIO.read(new File("Dane/kolor.jpg"));
                    sciezka_tekst.setText(selectedPfile.getPath());
                    obraz_wyswietlanie = obraz;
                }
                catch (IOException eee) {
                    eee.printStackTrace();
                }
            }
            obraz = resize(obraz,maksymalna_ilosc_pikseli_textfield.getText());
            obraz=filtrbw(obraz.getWidth(),obraz.getHeight(),obraz,Integer.parseInt(ilosc_kolorow_na_obrazku_textfield.getText())-1);
            obraz_wyswietlanie = resize(obraz,"300");
            obraz_wyswietlanie=filtrbw(obraz_wyswietlanie.getWidth(),obraz_wyswietlanie.getHeight(),obraz_wyswietlanie,Integer.parseInt(ilosc_kolorow_na_obrazku_textfield.getText())-1);
            obrazek.setImage(SwingFXUtils.toFXImage(obraz_wyswietlanie,null));
            sciezka_tekst.setText(selectedPfile.getPath());
            wymiary_tekst.setText(String.format("%.3f",(obraz.getWidth()*Double.parseDouble(srednica_czola_freza_textfield.getText())))+"mm"+" x "+
            		String.format("%.3f",(obraz.getHeight()*Double.parseDouble(srednica_czola_freza_textfield.getText())))+"mm");
            System.out.println("Działa");
        }
    }


}
