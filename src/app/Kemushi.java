package app;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class Kemushi extends Doubutsu {

	double skok = 0.1;

	public Kemushi(Yari ya, Yuka yu, BufferedImage e, Writer f) {
		super(ya, yu, e, f);

		skok = 0.05 * getYari().getD();
	}

	
	
	public void chokoku() {

		StringBuffer wyj = new StringBuffer();

		wyj.append(";;Początek Kemushi\n");

		try {

			double size_x = getManga().getWidth (null)*getYari().getD()*0.1;
			double size_y = getManga().getHeight(null)*getYari().getD()*0.1;

			double wsk1_x=0.0;
			double wsk1_y=0.0;
			double wsk2_x=0.0;
			double wsk2_y=0.0;

			boolean change = false;

			wyj.append("G1 X+").append(String.format ("%.2f", wsk1_x));
			wyj.append(  " Y+").append(String.format ("%.2f", wsk1_y));
			wyj.append(  " Z" ).append(String.format ("%d"  , this.getYuka().getGlebokosc_bialy()));
			wyj.append(  " F" ).append(String.format ("%d"  , this.getYuka().getPosow_roboczy())).append("\n");

			while((wsk1_x<size_x||wsk1_y<size_y)||(wsk2_x<size_x||wsk2_y<size_y)){
				if(wsk2_x<size_x){
					wsk2_x+=skok;
				} else {
					wsk2_y+=skok;
				}

				if (wsk1_y < size_y) {
					wsk1_y += skok;
				} else {
					wsk1_x += skok;
				}

				if(change){
					wyj.append("G1 X+").append(String.format ("%.2f", wsk2_x));
					wyj.append(  " Y+").append(String.format ("%.2f", wsk2_y));
					wyj.append(  " F" ).append(String.format ("%d"  , this.getYuka().getPosow_roboczy())).append("\n");
					wyj.append("G1 X+").append(String.format ("%.2f", wsk1_x));
					wyj.append(  " Y+").append(String.format ("%.2f", wsk1_y));
					wyj.append(  " F" ).append(String.format ("%d"  , this.getYuka().getPosow_roboczy())).append("\n");
				} else {
					wyj.append("G1 X+").append(String.format ("%.2f", wsk1_x));
					wyj.append(  " Y+").append(String.format ("%.2f", wsk1_y));
					wyj.append(  " F" ).append(String.format ("%d"  , this.getYuka().getPosow_roboczy())).append("\n");
					wyj.append("G1 X+").append(String.format ("%.2f", wsk2_x));
					wyj.append(  " Y+").append(String.format ("%.2f", wsk2_y));
					wyj.append(  " F" ).append(String.format ("%d"  , this.getYuka().getPosow_roboczy())).append("\n");
				}
				change=!change;
			}

			wyj.append("G0 Z+").append(String.format ("%d"  , this.getYuka().getGlebokosc_jalowy()));
			wyj.append(  " F" ).append(String.format ("%d"  , this.getYuka().getPosow_jalowy())).append("\n");

			wyj.append("G0 X+0.0");
			wyj.append(  " Y+0.0").append("\n");

			wyj.append("G0 Z+0.0").append("\n");
			wyj.append(  " F" ).append(String.format ("%d"  , this.getYuka().getPosow_jalowy())).append("\n");

			wyj.append(";;Koniec Kemushi\n");


			// Wrzucenie do pliku
			getKartka().append(wyj.toString());
			getKartka().flush();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}