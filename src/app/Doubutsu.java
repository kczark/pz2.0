package app;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public abstract class Doubutsu {

	private Yari yari;
	private BufferedImage manga;
	private Writer kartka;
	private Yuka yuka;

	public Yari getYari() {
		return yari;
	}

	public void setYari(Yari yari) {
		this.yari = yari;
	}

	public Writer getKartka() {
		return kartka;
	}

	public void setKartka(Writer k) {
		this.kartka = k;
	}

	public Yuka getYuka() {
		return yuka;
	}

	public void setYuka(Yuka yuka) {
		this.yuka = yuka;
	}

	public BufferedImage getManga() {
		return manga;
	}

	public void setManga(BufferedImage manga) {
		this.manga = manga;
	}


	public Doubutsu(Yari ya, Yuka yu, BufferedImage f, Writer e) {
		this.yari = ya;
		this.yuka = yu;
		this.kartka = e;
		this.manga = f;
	}

	public abstract void chokoku();
	
	
	public void Chokoku() {
		
		this.Booshi();
		
		if(getYuka().getWygladzanie()==1) {
			Rei rei=new Rei(getYari(),getYuka(),getManga(),getKartka());
			rei.chokoku();
		}
		if(getYuka().getWygladzanie()==2) {
			Makigai rei=new Makigai(getYari(),getYuka(),getManga(),getKartka());
			rei.chokoku();
		}
		if(getYuka().getWygladzanie()==3) {
			Kemushi rei=new Kemushi(getYari(),getYuka(),getManga(),getKartka());
			rei.chokoku();
		}
		if(getYuka().getWygladzanie()==4) {
			Kurage rei=new Kurage(getYari(),getYuka(),getManga(),getKartka());
			rei.chokoku();
		}
		
		
		this.chokoku();
		if(getYuka().getRamka()) {
			Osore rei=new Osore(getYari(),getYuka(),getManga(),getKartka());
			rei.chokoku();
		}
		this.Ashi();
	}
	
	
	public void Booshi()  {

		StringBuilder wyj = new StringBuilder(); // tworzenie napisu z kodem

		wyj.append(";;Sekwencja startowa:\n");
		wyj.append(";;Srednica czołowa freza (0.1mm):");
		wyj.append(yari.getD());
		wyj.append("\n;;");
		wyj.append("Kąt rozwarcia freza:");
		wyj.append(yari.getAlpha());
		wyj.append("\n;;");
		wyj.append("Wysokość części roboczej freza:");
		wyj.append(yari.getH());
		wyj.append("\n;;");
		wyj.append("Posów jałowy freza:");
		wyj.append(yuka.getPosow_jalowy());
		wyj.append("\n;;");
		wyj.append("Posów roboczy freza:");
		wyj.append(yuka.getPosow_roboczy());
		wyj.append("\n;;");
		wyj.append("Prędkość obrotowa freza:");
		wyj.append(yuka.getPredkosc_obrotowa());
		wyj.append("\n;;");
		wyj.append("Głębokość Czarny:");
		wyj.append(yuka.getGlebokosc_czarny());
		wyj.append("\n;;");
		wyj.append("Głębokość Jałowy:");
		wyj.append(yuka.getGlebokosc_jalowy());
		wyj.append("\n;;");
		wyj.append("Głębokość Biały:");
		wyj.append(yuka.getGlebokosc_bialy());
		wyj.append("\n;;");
		wyj.append("Czy frezować ramkę:");
		wyj.append(yuka.getRamka());
		wyj.append("\n;;");
		wyj.append("Wygładzanie:");
		switch (yuka.getWygladzanie()) {
			case 0:
				wyj.append("Brak");
				break;
			case 1:
				wyj.append("Liniowe");
				break;
			case 2:
				wyj.append("Spiralne");
				break;
			case 3:
				wyj.append("Skośne");
				break;
			case 4:
				wyj.append("Fraktal Peano");
				break;
		}
		wyj.append("\n;;");
		wyj.append("Czy frezować biały:");
		wyj.append(yuka.getFrezuj_bialy());
		wyj.append("\n");
		wyj.append("G21 G90 G64 G40\n");
		wyj.append("T0 M6\n");
		wyj.append("G17\n");
		wyj.append("M3 S").append(this.getYuka().getPredkosc_obrotowa()).append("\n");

		try {

            // Wrzucenie do pliku
			kartka.append(wyj.toString());
			kartka.flush();

		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void Ashi()  {

		StringBuilder wyj = new StringBuilder(); // tworzenie napisu z kodem

		try {
			wyj.append(";;sekwencja kończąca:\n");

			wyj.append("G0 Z+1.0 F"     ).append(this.getYuka().getPosow_jalowy()).append("\n");
			wyj.append("G0 X0.0 Y0.0  F").append(this.getYuka().getPosow_jalowy()).append("\n");
			wyj.append("G0 Z+0.0 F"     ).append(this.getYuka().getPosow_jalowy()).append("\n");
			wyj.append("M5\n");
			wyj.append("M30\n");

			// Wrzucenie do pliku
			kartka.append(wyj.toString());
			kartka.flush();

			// konec pracy z plikiem
			kartka.close();

		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
