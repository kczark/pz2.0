package app;

import java.awt.image.BufferedImage;
import java.io.*;

public class Osore extends Doubutsu {

	private double skok;

	public Osore(Yari ya, Yuka yu, BufferedImage e, Writer f) {
		super(ya, yu, e, f);

		skok = 0.05 * getYari().getD();
	}

	public void chokoku() {

		StringBuilder wyj = new StringBuilder(); // tworzenie napisu z kodem

		// tworzenie pliku wyjściowego
		
		try {
			// wrzucanie właściwego G-Kodu

			wyj.append("G1 X-").append(skok);
			wyj.append(  " Y-").append(skok);
			wyj.append(  " F" ).append(this.getYuka().getPosow_roboczy()).append("\n");

			wyj.append("G1 Z-").append((Double) (0.1d * this.getYuka().getGlebokosc_ramki()));
			wyj.append(  " F" ).append(this.getYuka().getPosow_roboczy()).append("\n");
			
			for(int j=-this.getYuka().getSzer_ramki();this.getYuka().getRamka()&&j<0;j++)
			{
				wyj.append("G1 X").append((Double) (j * skok));
				wyj.append(  " Y").append((Double) (j * skok));
				wyj.append(  " F").append(this.getYuka().getPosow_roboczy()).append("\n");
				
				wyj.append("G1 Y").append((2 * getManga().getHeight(null) - j) * skok);
				wyj.append(  " F").append(this.getYuka().getPosow_roboczy()).append("\n");
				wyj.append("G1 X").append((2 * getManga().getWidth (null) - j) * skok);
				wyj.append(  " F").append(this.getYuka().getPosow_roboczy()).append("\n");
			
				wyj.append("G1 Y").append(j * skok);
				wyj.append(  " F").append(this.getYuka().getPosow_roboczy()).append("\n");
			
				wyj.append("G1 X").append(j * skok);
				wyj.append(  " F").append(this.getYuka().getPosow_roboczy()).append("\n");
			}

			wyj.append("G0 Z+").append(String.format ("%d"  , this.getYuka().getGlebokosc_jalowy()));
			wyj.append(  " F" ).append(String.format ("%d"  , this.getYuka().getPosow_jalowy())).append("\n");

			wyj.append("G0 X+0.0");
			wyj.append(  " Y+0.0").append("\n");

			wyj.append("G0 Z+0.0").append("\n");
			wyj.append(  " F" ).append(String.format ("%d"  , this.getYuka().getPosow_jalowy())).append("\n");

			// Wrzucenie do pliku
			getKartka().append(wyj.toString());
			getKartka().flush();

		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}



}