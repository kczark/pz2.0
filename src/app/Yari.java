package app;

public class Yari {
    private int d=1;//srednica czola freza jednostka 0.1mm
    private int alpha=0;//kąt rozwarcia freza
    private int h=0;//wysokosc części roboczej freza w milimetrach

    public void setD(int d)
    {
        this.d=d;
    }

    public int getD()
    {
        return (this.d);
    }

    public void setAlpha(int alpha)
    {
        this.alpha=alpha;
    }

    public int getAlpha()
    {
        return (this.alpha);
    }

    public void setH(int h)
    {
        this.h=h;
    }

    public int getH()
    {
        return (this.h);
    }

}
